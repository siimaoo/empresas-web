import React from "react";
import Routes from 'utils/constants/routes';
import GlobalStyle from 'utils/styles/globalStyles';

function App() {
  return (
    <>
      <GlobalStyle />
      <Routes />
    </>
  );
}

export default App;

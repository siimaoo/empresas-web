import React from 'react';
import { useHistory } from "react-router-dom";
import { Company, CompanyList, Exception } from './style';

const Content = (props) => {
  const history = useHistory();

  const ResultsOfSearch = () => {
    if (props.filteredResults.length > 0) {
      return (
        <CompanyList>
          {props.filteredResults.map((result) => {
            return (
              <Company
                key={result.id}
                onClick={() => history.push(`/company/${result.id}`)}
              >
                <div>
                  <h4>{result.enterprise_name}</h4>
  
                  <h5>{result.enterprise_type.enterprise_type_name}</h5>
  
                  <h6>{result.country}</h6>
                </div>
              </Company>
            );
          })}
        </CompanyList>
      );
    } else {
      return(
        <Exception>
          Nenhuma empresa foi encontrada para a busca realizada.
        </Exception>
      );
    }
  }
  
  return(
    <ResultsOfSearch />
  );
}

export default Content;
import React from 'react';
import logo from "assets/images/logo-nav.png";
import logo2x from "assets/images/logo-nav@2x.png";
import logo3x from "assets/images/logo-nav@3x.png";
import searchIcon from "assets/images/icons/ic-search-copy.svg";
import { LogoNav, SearchIcon } from './style';

const NavBarContent = (props) => {
  return (
    <>
      <LogoNav
        src={logo}
        srcSet={`${logo2x} 2x, ${logo3x} 3x`}
        alt="ioasys"
      />

      <SearchIcon
        src={searchIcon}
        alt="search"
        height="30"
        className="search-icon"
        onClick={() => props.setIsSearching(!props.isSearching)}
      />
    </>
  );
}

export default NavBarContent;
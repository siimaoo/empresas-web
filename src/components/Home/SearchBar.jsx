import React from "react";
import searchIcon from "assets/images/icons/ic-search-copy.svg";
import closeIcon from "assets/images/icons/ic-close.svg";
import { Form } from './style';
import { Input, InputLeftIcon, InputRightIcon, InputWrapper } from 'components/core/Input';

const SearchBar = (props) => {
  return (
    <Form>
      <InputWrapper>
        <InputLeftIcon>
          <img src={searchIcon} alt="Search" height="30" className="icon" />
        </InputLeftIcon>

        <Input
          type="text"
          placeholder="Pesquisar"
          value={props.searchValue}
          onChange={(e) => props.setSearchValue(e.target.value)}
          borderColor="#fff"
          color="#fff"
        />

        <InputRightIcon>
          <img
            src={closeIcon}
            alt="Close Form"
            height="30"
            className="close-icon"
            onClick={() => props.setIsSearching(!props.isSearching)}
          />
        </InputRightIcon>
      </InputWrapper>
    </Form>
  );
};

export default SearchBar;

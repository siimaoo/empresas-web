import styled from 'styled-components';

const Company = styled.div `
  background-color: #fff;
  width: calc(100% - 60px);
  max-width: 500px;
  padding: 15px;
  margin-top: 15px;
  display: flex;
  flex-direction: row;
`;

const CompanyList = styled.div `
  width: 100%;
  margin-bottom: auto;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding-top: 75px;
`;

const Exception = styled.p `
  text-align: center;
  color: #b5b4b4;
`;

const LogoNav = styled.img `
  height: 40px;
  margin-left: auto;
  margin-right: -50px;
`;

const Form = styled.form `
  width: calc(100% - 20px);
`;

const SearchIcon = styled.img `
  margin-left: auto;
  margin-right: 20px;
  cursor: pointer;
`;



export { Company, CompanyList, Exception, LogoNav, Form, SearchIcon };
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import API from "config/api";
import emailIcon from "assets/images/icons/ic-email.png";
import emailIcon2x from "assets/images/icons/ic-email@2x.png";
import emailIcon3x from "assets/images/icons/ic-email@3x.png";
import lockIcon from "assets/images/icons/ic-cadeado.png";
import lockIcon2x from "assets/images/icons/ic-cadeado@2x.png";
import lockIcon3x from "assets/images/icons/ic-cadeado@3x.png";

import { AiFillExclamationCircle, AiOutlineEye, AiFillEye, AiOutlineLoading } from "react-icons/ai";

import { Input, InputLeftIcon, InputRightIcon, InputWrapper } from "components/core/Input";
import { Error, Button, Form, LoadingAnimation } from "pages/Login/style";

const LoginForm = () => {
  const history = useHistory();
  const [passwordIsVisible, setPasswordIsVisible] = useState(false);
  const [hasError, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const ShowPasswordButton = () => {
    return passwordIsVisible ? (
      <AiOutlineEye
        onClick={() => setPasswordIsVisible(!passwordIsVisible)}
      />
    ) : (
      <AiFillEye
        onClick={() => setPasswordIsVisible(!passwordIsVisible)}
      />
    );
  }

  const signin = async () => {
    setError(false);
    setIsLoading(true);

    const instance = new API();

    await instance.api
      .post("users/auth/sign_in", formData)
      .then((res) => {
        setIsLoading(false);
        const headers = res.headers;
        localStorage.setItem("client", headers.client);
        localStorage.setItem("uid", headers.uid);
        localStorage.setItem("access-token", headers["access-token"]);
        history.push("/home");
      })
      .catch((err) => {
        setIsLoading(false);
        if (!err.response.success) {
          setError(true);
        }
      });
  }

  return (
    <Form onSubmit={(e) => e.preventDefault()}>
      <InputWrapper>
        <InputLeftIcon>
          <img
            src={emailIcon}
            srcSet={`${emailIcon2x} 2x, ${emailIcon3x} 3x`}
            alt="ioasys"
          />
        </InputLeftIcon>

        <Input
          type="text"
          placeholder="E-mail"
          value={formData.email}
          onChange={(e) => {
            setFormData({ ...formData, email: e.target.value });
          }}
        />

        {hasError && (
          <InputRightIcon color="#ff0f44"><AiFillExclamationCircle /></InputRightIcon>
        )}
      </InputWrapper>

      <InputWrapper>
        <InputLeftIcon>
          <img
            src={lockIcon}
            srcSet={`${lockIcon2x} 2x, ${lockIcon3x} 3x`}
            alt="ioasys"
          />
        </InputLeftIcon>

        <Input
          type={passwordIsVisible ? "text" : "password"}
          placeholder="Senha"
          value={formData.password}
          onChange={(e) => {
            setFormData({ ...formData, password: e.target.value });
          }}
        />

        {hasError ? (
          <InputRightIcon color="#ff0f44"><AiFillExclamationCircle /></InputRightIcon>
        ) : (
          <InputRightIcon cursor="pointer">
            {ShowPasswordButton()}
          </InputRightIcon>
        )}
      </InputWrapper>

      {hasError && (
        <Error>Credenciais informadas são inválidas, tente novamente.</Error>
      )}

      <Button 
        type="submit" 
        onClick={() => signin()}
        disabled={isLoading}
      >
        { isLoading && <LoadingAnimation><AiOutlineLoading /></LoadingAnimation> } Entrar
      </Button>
    </Form>
  );
};

export default LoginForm;
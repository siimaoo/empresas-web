import styled from "styled-components";

const InputWrapper = styled.label`
  width: 100%;
`;

const Input = styled.input`
  width: 100%;
  height: 40px;
  border: none;
  outline: none;
  background-color: transparent;
  color: ${({ color }) => color};
  border-bottom: 1px solid ${({ borderColor }) => borderColor || '#383743'};
  font-size: 1.2rem;
  margin: 15px 0;
  padding-left: 40px;
  padding-right: 40px;
`;

const InputLeftIcon = styled.span`
  position: absolute;
  margin-top: 20px;
`;

const InputRightIcon = styled.span`
  position: absolute;
  margin-top: 20px;
  font-size: 1.5rem;
  margin-left: -30px;
  color: ${({ color }) => color};
  cursor: ${({ cursor }) => cursor};
`;

export { InputWrapper, Input, InputLeftIcon, InputRightIcon };

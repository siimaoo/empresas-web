import React from 'react';
import { Loading, LoadingAnimation } from './style';
import { AiOutlineLoading } from "react-icons/ai";

const LoadingPage = () => {
  return (
    <Loading>
      <LoadingAnimation>
        <AiOutlineLoading />
      </LoadingAnimation>
    </Loading>
  );
};

export default LoadingPage;
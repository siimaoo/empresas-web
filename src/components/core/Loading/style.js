import styled, { keyframes } from 'styled-components';

const Loading = styled.div `
  width: 100%;
  height: 100vh;
  background-color: rgba(255, 255, 255, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
`;

const LoadingKeyFrames = keyframes `
  0% {
    transform: rotate(0);
  }

  100% {
    transform: rotate(360deg);
  }
`;

const LoadingAnimation = styled.div `
  animation: ${LoadingKeyFrames} infinite 1s ease-in;
  color: #57bbbc;
  font-size: 2rem;
`;

export { Loading, LoadingAnimation }
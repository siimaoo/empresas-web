import styled from 'styled-components';

const NavBar = styled.nav `
  position: absolute;
  width: 100%;
  height: 75px;
  background: linear-gradient(180deg, rgba(238, 76, 119, 1) 24%, rgba(13, 4, 48, 1) 220%);
  display: flex;
  justify-content: ${({justifyContent}) => justifyContent || 'center'};
  align-items: center;
  color: ${({color}) => color};
  font-size: ${({fontSize}) => fontSize};
`;

export { NavBar };
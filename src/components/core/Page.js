import styled from 'styled-components';

const Page = styled.section `
  width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ebe9d7;
  flex-direction: column;
  color: #383743;
  padding-top: ${({padding}) => padding};
`;

export default Page;
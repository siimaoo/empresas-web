import axios from 'axios';

class API {
  api = axios.create({
    baseURL: 'https://empresas.ioasys.com.br/api/v1/'
  });

  constructor() {
    this.setAPIHeaders();
  }
  
  setAPIHeaders() {
    this.api.defaults.headers.common['Content-Type'] = 'application/json';
    this.api.defaults.headers.common['access-token'] = localStorage.getItem('access-token');
    this.api.defaults.headers.common['client'] = localStorage.getItem('client');
    this.api.defaults.headers.common['uid'] = localStorage.getItem('uid');
  }
}

export default API;
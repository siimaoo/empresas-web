import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { BsArrowLeftShort } from "react-icons/bs";
import API from "config/api";
import Page from "components/core/Page";
import { NavBar } from "components/core/NavBar";
import { Content } from './style';
import LoadPage from "components/core/Loading/Loading";

const Company = () => {
  const history = useHistory();
  const [companyData, setCompanyData] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const { id } = useParams();

  useEffect(() => {
    getCompanyData();
  }, []);

  const getCompanyData = async () => {
    const instance = new API();

    await instance.api
      .get(`enterprises/${id}`)
      .then(({ data }) => {
        setCompanyData(data.enterprise);
        setIsLoading(false);
      })
      .catch(() => {
        localStorage.clear();
        history.push('/login');
        setIsLoading(false);
      });
  };

  return (
    <>
     {
        isLoading ? <LoadPage /> :

        <>
          <NavBar
            justifyContent={'flex-start'}
            color="#fff"
            fontSize="1.6rem"
          >
            <div onClick={() => history.go(-1)}>
              <BsArrowLeftShort />
            </div>

            <p>{companyData.enterprise_name}</p>
          </NavBar>

          <Page>
            <Content>
              {/* <img src={`https://empresas.ioasys.com.br${companyData.photo}`} alt=""/> */}
              <p>{companyData.description}</p>
            </Content>
          </Page>
        </>
      }
    </>
  );
};

export default Company;

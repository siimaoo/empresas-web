import styled from 'styled-components';

const Content = styled.div `
  width: 90%;
  background-color: #fff;
  padding: 30px;
  max-width: 500px;
  border-radius: 2.4px;
  margin-top: 75px;
`;

export { Content };
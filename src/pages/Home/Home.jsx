import React, { useState, useEffect } from "react";
import API from "config/api";
import { NavBar } from "components/core/NavBar";
import NavBarContent from "components/Home/NavBarContent";
import SearchBar from "components/Home/SearchBar";
import Content from "components/Home/Content";
import Page from "components/core/Page";
import LoadPage from "components/core/Loading/Loading";
import { useHistory } from "react-router-dom";

const Home = () => {
  const history = useHistory();
  const [isSearching, setIsSearching] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [searchResults, setSerachResults] = useState([]);
  const [filteredResults, setFilteredResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    filterResults();
  }, [searchValue]);

  useEffect(() => {
    getCompany();
  }, []);

  const filterResults = () => {
    if (!searchValue) {
      setFilteredResults(searchResults);
      return;
    }

    const filtered = searchResults.filter((result) => {
      const regex = new RegExp(`${searchValue.toLowerCase()}`, "g");
      return result.enterprise_name.toLowerCase().match(regex);
    });

    setFilteredResults(filtered);
  }

  const getCompany = async () => {
    const instance = new API();

    await instance.api
      .get(`enterprises`)
      .then(({ data }) => {
        setSerachResults(data.enterprises);
        setFilteredResults(data.enterprises);
        setIsLoading(false);
      })
      .catch(() => {
        localStorage.clear();
        history.push('/login');
        setIsLoading(false);
      });
  }

  return (
    <>
      {
        isLoading ? <LoadPage /> :
        <>
          <NavBar>
            {isSearching ? (
              <SearchBar 
                searchValue={searchValue}
                isSearching={isSearching}
                setSearchValue={setSearchValue}
                setIsSearching={setIsSearching}
              />
            ) : (
              <NavBarContent
                isSearching={isSearching}
                setIsSearching={setIsSearching}
              />
            )}
          </NavBar>

          <Page>
            {!isSearching ? (
              <p>Clique na busca para iniciar.</p>
            ) : (
              <Content filteredResults={filteredResults} />
            )}
          </Page>
        </>
      }
    </>
  );
};

export default Home;
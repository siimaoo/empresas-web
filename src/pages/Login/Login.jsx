import React from "react";
import logo from "assets/images/logo-home.png";
import logo2x from "assets/images/logo-home@2x.png";
import logo3x from "assets/images/logo-home@3x.png";
import Page from "components/core/Page";
import LoginForm from "components/Login/Login-Form";
import { LoginHeader, Logo, Title, SubTitle } from "./style";

const Login = () => {
  return (
    <Page>
      <LoginHeader>
        <Logo
          src={logo}
          srcSet={`${logo2x} 2x, ${logo3x} 3x`}
          alt="ioasys"
        />

        <Title>BEM-VINDO AO EMPRESAS</Title>

        <SubTitle>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </SubTitle>
      </LoginHeader>

      <LoginForm />
    </Page>
  );
};

export default Login;

import styled, { keyframes } from 'styled-components';

const Error = styled.span `
  color: #ff0f44;
`;

const Button = styled.button `
  color: #fff;
  background-color: #57bbbc;
  border-radius: 1.8px;
  border: none;
  outline: none;
  height: 50px;
  width: 100%;
  max-width: 300px;
  cursor: pointer;
  font-size: 1.2rem;
  font-weight: bold;
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;

  :disabled {
    background-color: #8dd8d9;
  }
`;

const Form = styled.form `
  width: 90%;
  max-width: 400px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: 30px;
`;

const LoginHeader = styled.div `
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  text-align: center;
  padding: 15px;
`;

const Logo = styled.img `
  margin: 7.5px;
`;

const Title = styled.h2 `
  margin: 7.5px;
`;

const SubTitle = styled.p `
  margin: 7.5px;
`;

const Loading = keyframes `
  0% {
    transform: rotate(0);
  }

  100% {
    transform: rotate(360deg);
  }
`;

const LoadingAnimation = styled.div `
  animation: ${Loading} infinite 1s ease-in;
  margin-right: 10px;
`;


export { Error, Button, Form, LoginHeader, Logo, Title, SubTitle, LoadingAnimation };
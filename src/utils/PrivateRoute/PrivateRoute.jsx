import React from "react";
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const hasToken = localStorage.getItem('access-token');
  const hasClient = localStorage.getItem('client');
  const hasUID = localStorage.getItem('uid');

  return (
    <Route
      {...rest}
      render={(props) =>
        hasToken && hasClient && hasUID ? (<Component {...props} />) : (<Redirect to={{pathname: "/login"}} />)
      }
    />
  );
};

export default PrivateRoute;

import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "pages/Home";
import Login from 'pages/Login';
import Company from 'pages/Company';
import PrivateRoute from 'utils/PrivateRoute';
import { HOME, LOGIN, COMPANY } from './paths';

const Routes = () => (
  <Router>
    <Switch>
      <PrivateRoute exact path={HOME} component={Home} />
      <Route exact path={LOGIN} component={Login} />
      <PrivateRoute exact path={`${COMPANY}/:id`} component={Company} />
      <Redirect exact from="*" to="home" />
    </Switch>
  </Router>
);
    
export default Routes;
  